package com.epam.lab.database.service;

import java.util.List;

import com.epam.lab.database.dao.MessageDAO;
import com.epam.lab.database.model.Message;


public class MessageService {
	
	public static void addMessage(Message message){
		MessageDAO.addMessag(message);
	}
	
	public static void delMessage(Integer id){
		MessageDAO.delMessage(id);
	}
	
	public static Message getMessage(Integer id){
		return MessageDAO.getMessage(id);
	}
	
	public static List<Message> getAllMessage(Integer reciver){
		return MessageDAO.getAllMessags(reciver);
	}
	

}

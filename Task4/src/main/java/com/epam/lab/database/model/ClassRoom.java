package com.epam.lab.database.model;

public class ClassRoom {

	private Integer id;
	private Integer student;
	private Integer lector;
	private Integer subject;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStudent() {
		return student;
	}
	public void setStudent(Integer student) {
		this.student = student;
	}
	public Integer getLector() {
		return lector;
	}
	public void setLector(Integer lector) {
		this.lector = lector;
	}
	public Integer getSubject() {
		return subject;
	}
	public void setSubject(Integer subject) {
		this.subject = subject;
	}
	@Override
	public String toString() {
		return "Class [id=" + id + ", student=" + student + ", lector="
				+ lector + ", subject=" + subject + "]";
	};
	
	
}

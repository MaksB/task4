package com.epam.lab.database.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.epam.lab.database.service.ClassService;

public class Student implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String email;
	private String password;
	private String name;
	private String surname;
	private String country;
	private String adress;
	private Gender gender;
	private Date date;
	private String url;
	private Boolean status;
	private Boolean verificate;
	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getGender() {
		return gender.name().toLowerCase();
	}

	public void setGender(String gender) {
		try {
			this.gender = Gender.valueOf(gender.toUpperCase());
		} catch (IllegalArgumentException e) {

		}

	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public enum Gender {
		MALE, FAMALE;
	}

	public boolean isStudyCours(int id){
		List<ClassRoom> list= ClassService.getAllClassRoom();
		for (ClassRoom classRoom : list) {
			if (classRoom.getStudent()==getId()) {
				if(classRoom.getSubject()== id){
					return true;
				}
			}
		}
		return false;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	
	public Boolean getVerificate() {
		return verificate;
	}

	public void setVerificate(Boolean verificate) {
		this.verificate = verificate;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", email=" + email + ", password="
				+ password + ", name=" + name + ", surname=" + surname
				+ ", country=" + country + ", adress=" + adress + ", gender="
				+ gender + ", date=" + date + ", url=" + url + ", status="
				+ status + ", verificate=" + verificate + "]";
	}

	

	

}

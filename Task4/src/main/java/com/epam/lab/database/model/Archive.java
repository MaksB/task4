package com.epam.lab.database.model;

import java.util.Date;



public class Archive {
	private Integer id;
	private Integer student;
	private Integer lector;
	private Integer rating;
	private Integer subject;
	private Date date;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStudent() {
		return student;
	}

	public void setStudent(Integer student) {
		this.student = student;
	}

	public Integer getLector() {
		return lector;
	}

	public void setLector(Integer lector) {
		this.lector = lector;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Integer getSubject() {
		return subject;
	}

	public void setSubject(Integer subject) {
		this.subject = subject;
	}
	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Archive [id=" + id + ", student=" + student + ", lector="
				+ lector + ", rating=" + rating + ", subject=" + subject
				+ ", date=" + date + "]";
	}

	

}
